//!Provides you with global tokio runtimes
//!
//!It can be used from any point in code without need to manually pass around runtime itself
//!Or being forced to use tokio macros
//!
//!## Example
//!
//!```rust
//!use tokio_global::{Runtime, AutoRuntime};
//!use tokio::io::{AsyncWriteExt, AsyncReadExt};
//!
//!async fn server() {
//!    let mut listener = tokio::net::TcpListener::bind("127.0.0.1:8080").await.expect("To bind");
//!
//!    let (mut socket, _) = listener.accept().await.expect("To accept connection");
//!
//!    async move {
//!        let mut buf = [0; 1024];
//!        loop {
//!            match socket.read(&mut buf).await {
//!                // socket closed
//!                Ok(0) => return,
//!                Ok(_) => continue,
//!                Err(_) => panic!("Error :("),
//!            };
//!        }
//!    }.spawn().await.expect("Finish listening");
//!}
//!
//!async fn client() {
//!    let mut stream = tokio::net::TcpStream::connect("127.0.0.1:8080").await.expect("Connect");
//!
//!    // Write some data.
//!    stream.write_all(b"hello world!").await.expect("Write");
//!
//!    //Stop runtime
//!    Runtime::stop();
//!}
//!
//!let _guard = Runtime::default();
//!
//!let runner = std::thread::spawn(|| {
//!    Runtime::run();
//!});
//!
//!server().spawn();
//!client().spawn();

pub extern crate tokio;

///Trait to bootstrap your futures.
pub trait AutoRuntime: core::future::Future {
    ///Blocks and waits for future to finish.
    fn wait(self) -> Self::Output;

    ///Spawns futures onto global runtime.
    ///
    ///## Note
    ///
    ///Can be used in any context, but it requires future that returns nothing
    fn spawn(self) -> tokio::task::JoinHandle<Self::Output> where Self: Send + core::future::Future + 'static, Self::Output: Send + 'static;
}

mod blocking;
mod guard;
mod global;
pub use global::Runtime;
