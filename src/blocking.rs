use core::sync::atomic::{AtomicBool, Ordering};

pub struct Block {
    thread: std::thread::Thread,
    wake: AtomicBool,
}

impl Block {
    pub fn new() -> Self {
        Self {
            thread: std::thread::current(),
            wake: AtomicBool::new(false),
        }
    }

    pub fn split(&self) -> (Signal, Lock<'_>) {
        (Signal(unsafe { core::mem::transmute(self) }), Lock(self))
    }
}

pub struct Signal(&'static Block);

impl Signal {
    pub fn signal(&self) -> bool {
        let wake = !self.0.wake.compare_and_swap(false, true, Ordering::SeqCst);

        if wake {
            self.0.thread.unpark();
        }

        wake
    }
}

impl Drop for Signal {
    fn drop(&mut self) {
        if !std::thread::panicking() {
            assert!(self.0.wake.load(Ordering::Relaxed), "UNPARK BEFORE DROPPING!");
        }
    }
}

pub struct Lock<'a>(&'a Block);

impl Lock<'_> {
    pub fn wait(&self) {
        //Just in case make sure that current thread is the same as original
        //But we do not expose it to user, so debug only
        debug_assert_eq!(self.0.thread.id(), std::thread::current().id());

        while !self.0.wake.load(Ordering::SeqCst) {
            std::thread::park();
        }
    }
}

static WAKER: core::task::RawWakerVTable = core::task::RawWakerVTable::new(vtable::clone, vtable::wake, vtable::wake_by_ref, vtable::drop);

pub fn create_waker(signal: &Signal) -> core::task::Waker {
    let raw = core::task::RawWaker::new(signal as *const Signal as *const _, &WAKER);
    unsafe {
        core::task::Waker::from_raw(raw)
    }
}

mod vtable {
    use super::{Signal, WAKER};

    pub unsafe fn clone(data: *const ()) -> core::task::RawWaker {
        core::task::RawWaker::new(data, &WAKER)
    }

    pub unsafe fn wake(data: *const ()) {
        (*(data as *const Signal)).signal();
    }

    pub unsafe fn wake_by_ref(data: *const ()) {
        (*(data as *const Signal)).signal();
    }

    pub unsafe fn drop(data: *const ()) {
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic]
    fn blocking_panics_when_not_signaled() {
        Block::new().split();

    }

    #[test]
    fn blocking_ok_after_signal() {
        let block = Block::new();
        let (signal, lock) = block.split();

        std::thread::spawn(move || {
            signal.signal();
        });
        lock.wait();
    }
}
