# tokio-global

[![Build](https://gitlab.com/Douman/tokio-global/badges/master/pipeline.svg)](https://gitlab.com/Douman/tokio-global/pipelines)
[![Crates.io](https://img.shields.io/crates/v/tokio-global.svg)](https://crates.io/crates/tokio-global)
[![Documentation](https://docs.rs/tokio-global/badge.svg)](https://docs.rs/crate/tokio-global/)

Simple way to create global tokio runtime, available from any place in code

## Usage

Start runtime and use `AutoRuntime` trait to spawn your futures:

```rust
use tokio_global::{Runtime, AutoRuntime};
use tokio::io::{AsyncWriteExt, AsyncReadExt};

async fn server() {
    let mut listener = tokio::net::TcpListener::bind("127.0.0.1:8080").await.expect("To bind");

    let (mut socket, _) = listener.accept().await.expect("To accept connection");

    async move {
        let mut buf = [0; 1024];
        loop {
            match socket.read(&mut buf).await {
                // socket closed
                Ok(0) => return,
                Ok(_) => continue,
                Err(_) => panic!("Error :("),
            };
        }
    }.spawn().await.expect("Finish listening");
}

async fn client() {
    let mut stream = tokio::net::TcpStream::connect("127.0.0.1:8080").await.expect("Connect");

    // Write some data.
    stream.write_all(b"hello world!").await.expect("Write");

    //Stop runtime
    Runtime::stop();
}

let _guard = Runtime::default();

let runner = std::thread::spawn(|| {
    Runtime::run();
});

server().spawn();
client().spawn();
```
